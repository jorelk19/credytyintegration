﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TestCredytyIntegrationService.Business;
using TestCredytyIntegrationService.Models;

namespace TestCredytyIntegrationService.Controllers
{
    [Route("api/IntegrationService")]
    [ApiController]
    public class IntegrationServiceController : ControllerBase
    {        
        [HttpGet]
        [Route("GetUsers")]
        public IActionResult GetUsers()
        {
            var result = ServiceManager.Instancia.GetAllUsers();
            return Ok(result);
        }

        [HttpGet]
        [Route("GetUser/{userId}")]
        public IActionResult GetUser(int userId)
        {
            var result = ServiceManager.Instancia.GetUser(userId);
            return Ok(result);
        }

        [HttpPost]
        [Route("CreateUser")]
        public IActionResult CreateUser(User user)
        {
            var result = ServiceManager.Instancia.CreateUser(user);
            return Ok(result);
        }

        [HttpPut]
        [Route("UpdateUser")]
        public IActionResult UpdateUser(User user)
        {
            var result = ServiceManager.Instancia.UpdateUser(user);
            return Ok(result);
        }

        [HttpDelete]
        [Route("DeleteUser/{userId}")]
        public IActionResult DeleteUser(int userId)
        {
            var result = ServiceManager.Instancia.DeleteUser(userId);
            return Ok(result);
        }
    }
}
