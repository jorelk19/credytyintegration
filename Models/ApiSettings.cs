﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCredytyIntegrationService.Models
{
    public class ApiSettings
    {
        private static IConfiguration configurationService { get; set; }
        private static IWebHostEnvironment environment { get; set; }

        private static ServiceConnection serviceConnection { get; set; }
        public static ServiceConnection ServiceConnection { get { return serviceConnection; } }
        internal static void ReadSettings(IConfiguration configuration)
        {
            serviceConnection = configuration.GetSection("UserServices").Get<ServiceConnection>();
        }

    }
}
