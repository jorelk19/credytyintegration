﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCredytyIntegrationService.Models
{
    public class ServiceConnection
    {
        public string GetAllUsersService { get; set; }        
        public string GetUserService { get; set; }
        public string CreateUserService { get; set; }
        public string DeleteUserService { get; set; }
        public string UpdateUserService { get; set; }
    }
}
