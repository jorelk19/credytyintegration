﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using TestCredytyIntegrationService.Models;
using TestCredytyIntegrationService.Utilities;

namespace TestCredytyIntegrationService.Business
{
    public class ServiceManager
    {
        /// <summary>
        /// Private var to get the instance to manage the singleton
        /// </summary>
        private static ServiceManager instance;

        /// <summary>
        /// Instance to manage the singleton for the class
        /// </summary>
        public static ServiceManager Instancia
        {
            get
            {
                if (instance == null)
                {
                    instance = new ServiceManager();
                }
                return instance;
            }
        }

        public List<User> GetAllUsers()
        {
            return RestConnectionManager.Instance.GetService<List<User>>(ApiSettings.ServiceConnection.GetAllUsersService, "/getallusers", Method.GET);
        }

        public User GetUser(int userId)
        {
            var endpoint = string.Format("/GetUser/{0}", userId);
            return RestConnectionManager.Instance.GetService<User>(ApiSettings.ServiceConnection.GetUserService, endpoint, Method.GET);
        }

        public bool CreateUser(User user)
        {
            return RestConnectionManager.Instance.SendService<bool>(ApiSettings.ServiceConnection.CreateUserService, "/CreateUser", Method.POST, user);
        }

        public bool UpdateUser(User user)
        {
            return RestConnectionManager.Instance.SendService<bool>(ApiSettings.ServiceConnection.UpdateUserService, "/Update", Method.PUT, user);
        }

        public bool DeleteUser(int userId)
        {
            var endpoint = string.Format("/DeleteUser/{0}", userId);
            return RestConnectionManager.Instance.GetService<bool>(ApiSettings.ServiceConnection.DeleteUserService, endpoint, Method.DELETE);
        }
    }
}
